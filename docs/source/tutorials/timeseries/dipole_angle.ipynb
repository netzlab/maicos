{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dipole angle tutorial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we analyse a box of water molecules exposed to an alternating electric field. \n",
    "To follow this tutorial, the data test files `electricfwater` of MAICoS are needed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Simulation details - The system contains $\\sim 5000$ water molecules simulated \n",
    "for 100 ps in the NVT ensemble at a temperature of 300 K. Periodic \n",
    "boundary conditions were employed in all directions and long range \n",
    "electrostatics were modelled using the PME method.\n",
    "LINCS algorithm was used to constraint the H-Bonds. The alternating electric field\n",
    "was applied along the $x$-axis in the form of a Gaussian laser pulse. \n",
    "Please check \n",
    "[gromacs electric field](https://manual.gromacs.org/2019-current/reference-manual/special/electric-fields.html)\n",
    "for more details."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To follow this tutorial, the data test files airwater of MAICoS are needed. You can obtain them by cloning MAICoS repository:\n",
    "```\n",
    "    git clone git@gitlab.com:maicos-devel/maicos.git\n",
    "```\n",
    "The ``electricfwater`` data files are located in ``tests/data/electricfwater/``."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Option 1: from the Python interpreter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let us ignore unnecessary warnings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, import MAICoS, NumPy, MDAnalysis, and PyPlot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import maicos\n",
    "import numpy as np\n",
    "import MDAnalysis as mda\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.ticker import AutoMinorLocator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and set a few parameters for plotting purpose:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fontsize = 25\n",
    "font = {'family': 'sans', 'color':  'black', \n",
    "        'weight': 'normal', 'size': fontsize}\n",
    "my_color_1 = np.array([0.090, 0.247, 0.560])\n",
    "my_color_2 = np.array([0.235, 0.682, 0.639])\n",
    "my_color_3 = np.array([1.000, 0.509, 0.333])\n",
    "my_color_4 = np.array([0.588, 0.588, 0.588])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define the path to the ``electricfwater`` data folder of MAICoS:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "datapath = \"../../../../tests/data/electricfwater/\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a MDAnalysis universe, and extract its atoms:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = mda.Universe(datapath+\"mdelectric.tpr\",\n",
    "                 datapath+\"mdelectric.trr\")\n",
    "atoms = u.atoms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Important note: Because this calculation is computationaly heavy, \n",
    "let use isolate a small portion of the trajectory file. To perform the full \n",
    "calculation, just comment the following line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u.transfer_to_memory(stop = 100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us print a few information about the trajectory file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"The number of water molecules is {np.int32(u.atoms.n_atoms/3)}\")\n",
    "timestep = np.round(u.trajectory.dt,2)\n",
    "print(f\"The time interval between the frames is {timestep} ps\")\n",
    "total_time = np.round(u.trajectory.totaltime,2)\n",
    "print(f\"The total simulation time is {total_time} ps\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the MAICoS dipole angle function, selecting the $x$\n",
    "axis using the `dim` keyword:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dipangle = maicos.DipoleAngle(atoms, dim=0)\n",
    "dipangle.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The run produces a python dictionary named ``dipangle.results``\n",
    "with 4 keys linked to numpy arrays as values: \n",
    "1. timestep, \n",
    "2. cosine of dipole and x-axis, \n",
    "3. cosine squared, \n",
    "4. product of cosine of dipoles $i$ and $j$ with $i \\ne j$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Extract the time vector and the $\\cos(\\theta_i)$ data \n",
    "from the ``results`` attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = dipangle.results[\"t\"]\n",
    "cos_theta_i = dipangle.results[\"cos_theta_i\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " Plot the final results using :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(13,6.5))\n",
    "ax1 = plt.subplot(1, 1, 1)\n",
    "plt.xlabel(r\"$t$ (ps)\", fontdict=font)\n",
    "plt.ylabel(r\"cos($\\theta_i$)\", fontdict=font)\n",
    "plt.xticks(fontsize=fontsize)\n",
    "plt.yticks(fontsize=fontsize)\n",
    "ax1.plot(t, cos_theta_i, color=my_color_1, linewidth=4)\n",
    "ax1.yaxis.offsetText.set_fontsize(20)\n",
    "ax1.minorticks_on()\n",
    "ax1.tick_params('both', length=10, width=2, which='major', direction='in')\n",
    "ax1.tick_params('both', length=6, width=1.4, which='minor', direction='in')\n",
    "ax1.xaxis.set_ticks_position('both')\n",
    "ax1.yaxis.set_ticks_position('both')\n",
    "ax1.spines[\"top\"].set_linewidth(2)\n",
    "ax1.spines[\"bottom\"].set_linewidth(2)\n",
    "ax1.spines[\"left\"].set_linewidth(2)\n",
    "ax1.spines[\"right\"].set_linewidth(2)\n",
    "ax1.yaxis.offsetText.set_fontsize(30)\n",
    "minor_locator_y = AutoMinorLocator(2)\n",
    "ax1.yaxis.set_minor_locator(minor_locator_y)\n",
    "minor_locator_x = AutoMinorLocator(2)\n",
    "ax1.xaxis.set_minor_locator(minor_locator_x)\n",
    "ax1.tick_params(axis='x', pad=10)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Option 2: from the command line interface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Go to the ``electricfwater`` data folder of MAICoS, then use the ``maicos``\n",
    "command directly from the terminal:\n",
    "```\n",
    "    cd tests/data/electricfwater/\n",
    "    maicos DipoleAngle -s md.tpr -f md.trr -d 0\n",
    "```\n",
    "\n",
    "The output file ``dipangle.dat`` is similar to ``dipangle.results`` and contains the data in columns. To know the full\n",
    "list of options, have a look at the ``Inputs``."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
