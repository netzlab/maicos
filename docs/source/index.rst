MAICoS
======

.. image:: images/logo_MAICOS_square.png
   :align: right
   :width: 100

.. include:: ../../README.rst
   :start-after: inclusion-readme-intro-start
   :end-before: inclusion-readme-intro-end

.. include:: ../../README.rst
   :start-after: inclusion-readme-start
   :end-before: inclusion-readme-end

.. toctree::
   :maxdepth: 4
   :caption: MAICoS
   :hidden:

   ./documentation_pages/installation
   ./documentation_pages/usage
   ./documentation_pages/tutorials
   ./documentation_pages/gettinginvolved
   ./documentation_pages/authors
   ./documentation_pages/changelog
   
.. toctree::
   :maxdepth: 4
   :caption: Modules
   :hidden:
   
   ./documentation_pages/modules/density
   ./documentation_pages/modules/dielectric
   ./documentation_pages/modules/structure
   ./documentation_pages/modules/timeseries
   ./documentation_pages/modules/transport
