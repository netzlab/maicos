========
Velocity
========

.. automodule:: maicos.modules.transport.Velocity
    :members:
    :undoc-members:
    :show-inheritance:
