==============
Kinetic energy
==============

.. automodule:: maicos.modules.timeseries.KineticEnergy
    :members:
    :undoc-members:
    :show-inheritance:
