============
Dipole angle
============

.. automodule:: maicos.modules.timeseries.DipoleAngle
    :members:
    :undoc-members:
    :show-inheritance:
