===================
Dielectric spectrum
===================

.. automodule:: maicos.modules.epsilon.DielectricSpectrum
    :members:
    :undoc-members:
    :show-inheritance:
