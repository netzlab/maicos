==================
Timeseries modules
==================

.. automodule:: maicos.modules.timeseries

.. toctree::
   :titlesonly:
   :maxdepth: 4
   :caption: Timeseries modules
   :hidden:
   
   dipoleangle
   kineticenergy

