.. _label_installation:

============
Installation
============

`Python3`_ and a C-compiler are needed to build the
underlying libraries. Install the package using 
`pip`_ with:

.. code-block:: bash

    pip3 install numpy
    pip3 install maicos

Alternatively, if you don't have special privileges, install
the package only for the current using the ``--user`` flag:

.. code-block:: bash

    pip3 install --user numpy
    pip3 install --user maicos

.. _`Python3`: https://www.python.org
.. _pip: http://www.pip-installer.org/en/latest/index.html


.. toctree::
   :maxdepth: 4
   :hidden:
   :titlesonly:
